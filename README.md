## 关于项目 

 本项目是由 [小幺鸡](https://gitee.com/zhoujingjie/apiManager) forked而来.
 原项目作者已经不再维护了.昨天发现有一部分资源不可用,发现作者的七牛云已经过期不再提供服务了.
 所以我分出这个分支来修改里面的一些bug,项目是好项目,可惜了

## 安装部署
1. 导入mysql数据库 xiaoyaoji-web/resources/xiaoyaoji.sql
2. 修改 xiaoyaoji-web/resources/config.properties的数据库连接
3. 启动项目

## 插件开发
* 目前插件类型有导入，导出，文档三种类型
* 可以参照xiaoyaoji-pdf 或者 xiaoyaoji-plugins 模块
### 文档插件
1. 实现cn.com.xiaoyaoji.core.plugin.doc.DocEvPlugin接口
2. 插件目录（采用标准的maven目录结构）
   --- xiaoyaoji-plugins  
   ------ src  
   --------- main  
   -------------- java  
   ------------------ com.xxxxx  
   -------------- plugins-resources  
   ------------------ web （静态页面）  
   ------------------ plugin.json
3. 打包插件： 采用apache的 maven-assembly-plugin 插件。 具体配置参考已有插件配置
4. 打包方法。 mvn package；
